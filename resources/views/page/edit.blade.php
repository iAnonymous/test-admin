@extends('layouts.master')

@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Edit page</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <form class="form-horizontal form-label-left" method="post" action="{{ route('page.update', $page->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit page</h2>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="col-md-9 col-sm-9 col-xs-9">
                    <label>Title</label>
                    <input type="text" id="title" class="form-control" name="title" required="" value="{{ (old('title')) ? old('title') : $page->title }}">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3xdisplay_inputx form-group has-feedback">
                        <label>Publish Date</label>
                        <input type="text" required="" name="publish_date" readonly="" class="form-control has-feedback-left" id="single_cal1" placeholder="Publish Date" aria-describedby="inputSuccess2Status" value="{{ (old('publish_date')) ? old('publish_date') : $page->publish_date->format('m/d/Y') }}">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        <span id="inputSuccess2Status" class="sr-only">(success)</span>
                    </div>

                    <div class="x_content">


                        <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#content">
                            <div class="btn-group">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa icon-font"></i><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a>
                                    </li>
                                    <li><a data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a>
                                    </li>
                                    <li><a data-edit="fontSize 1"><p style="font-size:11px">Small</p></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                                <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                                <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
                                <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                            </div>
                            <div class="btn-group">
                                <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
                                <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
                                <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                                <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                            </div>
                            <div class="btn-group">
                                <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                                <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                                <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                                <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                            </div>
                            <div class="btn-group">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>
                                <div class="dropdown-menu input-append">
                                    <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                    <button class="btn" type="button">Add</button>
                                </div>
                                <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>
                            </div>
                            <div class="btn-group">
                                <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                                <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                            </div>
                            <div class="btn-group">
                                <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                                <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                            </div>
                        </div>

                        <div id="editor">
                        	
                        	{{ (old('content')) ? old('content') : $page->content }}
                        </div>
                        <textarea name="content" id="content" style="display:none;"></textarea>

                        
   
                        <br />
                     <div class="form-group">

                     	@if ($page->image)
                     	<img src="{{ asset('uploads/thumb/'.$page->image) }}" class="thumbnail">
                     	@endif

                        <label>Photo</label>
                        <input type="file" name="photo" class="form-control" placeholder="Photo">
                        </div>
                        <div class="ln_solid"></div>
                        


                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                <a href="{{ route('page.index') }}" class="btn btn-primary">Cancel</a>
                                <button type="submit" class="btn btn-success saveContent">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection