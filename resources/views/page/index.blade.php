@extends('layouts.master')

@section('content')
    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>
                    Tables
                    <small>
                        Some examples to get you started
                    </small>
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form method="get">
                    <div class="input-group">
                        
                        <input type="text" class="form-control" name="search" value="{{ Request::get('search') }}" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                        </span>
                        
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Basic Tables <small>basic table subtitle</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>image</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Author</th>
                                    <th>Publish date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pages as $page)
                                <tr>
                                    <th scope="row">{{ $page->id }}</th>
                                    <td><img src="{{ ($page->image) ? asset('uploads/thumb/'.$page->image) : asset('uploads/thumb/default.png') }}" width="40" height="40"></td>
                                    <td>{{ $page->title }}</td>
                                    <td>{{ str_limit(strip_tags($page->content),$limit = 250, $end = '...') }}</td>
                                    <td>{{ $page->user->first_name }} {{ $page->user->last_name }}</td>
                                    <td>{{ $page->publish_date }}</td>
                                    <td>
                                        <a href="{{ route('page.edit', $page->id) }}" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                        <a href="{{ route('page.destroy', $page->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete?');"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                        {{ $pages->links() }}
                    </div>
                </div>
            </div>


        </div>

    </div>

@endsection