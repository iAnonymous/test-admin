<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('auth/logout', 'Auth\AuthController@logout');

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'page'], function () {
	Route::get('/', 'PageController@index')->name('page.index');

	Route::get('/create', 'PageController@create')->name('page.create');
	Route::post('/', 'PageController@store')->name('page.store');

	Route::get('/{id}/edit', 'PageController@edit')->name('page.edit');
	Route::post('/{id}', 'PageController@update')->name('page.update');

	Route::get('/{id}/delete', 'PageController@destroy')->name('page.destroy');
});


Route::group(['prefix' => 'user'], function () {
	Route::get('/', 'UserController@index')->name('user.index');

	Route::get('/create', 'UserController@create')->name('user.create');
	Route::post('/', 'UserController@store')->name('user.store');

	Route::get('/{id}/edit', 'UserController@edit')->name('user.edit');
	Route::post('/{id}', 'UserController@update')->name('user.update');

	Route::get('/{id}/delete', 'UserController@destroy')->name('user.destroy');
});