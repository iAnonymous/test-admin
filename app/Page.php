<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['user_id', 'title', 'content', 'publish_date'];
    protected $dates = ['publish_date'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
