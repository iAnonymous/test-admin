<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Page extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['user_id', 'title', 'content', 'publish_date'];
    protected $dates = ['publish_date'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
