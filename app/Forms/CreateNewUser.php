<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CreateNewUser extends Form
{
    public function buildForm()
    {
        $this
            ->add('first_name', 'text', [
                'rules' => 'required|min:5',
            ])
            ->add('last_name', 'text', [
                'rules' => 'required|min:5',
            ])
            ->add('email', 'email', [
                'rules' => 'required|min:5',
            ])
            ->add('password', 'password', [
                'rules' => 'required|min:5',
            ])
            ->add('confirm_password', 'password', [
                'rules' => 'required|min:5',
            ]);
    }
}
