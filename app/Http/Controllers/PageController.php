<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PageCreateRequest;
use App\Http\Requests\PageUpdateRequest;
use App\Repositories\PageRepository;
use App\Validators\PageValidator;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Image;

class PageController extends Controller
{

    /**
     * @var PageRepository
     */
    protected $repository;

    /**
     * @var PageValidator
     */
    protected $validator;

    public function __construct(PageRepository $repository, PageValidator $validator)
    {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pages = $this->repository->with(['user'])
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('page.index', compact('pages'));
    }

    public function create()
    {
        return view('page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PageCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PageCreateRequest $request)
    {

        try {

            $page = $this->repository->create([
                'user_id'      => Auth::id(),
                'title'        => $request->get('title'),
                'content'      => $request->get('content'),
                'publish_date' => date('Y-m-d', strtotime($request->get('publish_date')))
            ]);

            if ($request->hasFile('photo')) {

                $photo     = $request->file('photo');
                $name      = $photo->getClientOriginalName();
                $extension = $photo->getClientOriginalExtension();
                $name      = md5($name.time()).'.'.$extension;

                $img = Image::make($photo);
                $img->save(public_path('uploads/'.$name));

                $img->resize(50, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->crop(50, 50, 0, 0);
                $img->save(public_path('uploads/thumb/'.$name));

                $page->image = $name;
                $page->save();

            }

            return redirect()->route('page.index');

        } catch (ValidatorException $e) {
  
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $page,
            ]);
        }

        return view('page.show', compact('page'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $page = $this->repository->find($id);

        return view('page.edit', compact('page'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  PageUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PageUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $page = $this->repository->update([
                'title'        => $request->get('title'),
                'content'      => $request->get('content'),
                'publish_date' => date('Y-m-d', strtotime($request->get('publish_date')))
            ], $id);

            if ($request->hasFile('photo')) {

                $photo     = $request->file('photo');
                $name      = $photo->getClientOriginalName();
                $extension = $photo->getClientOriginalExtension();
                $name      = md5($name.time()).'.'.$extension;

                $img = Image::make($photo);
                $img->save(public_path('uploads/'.$name));

                $img->resize(50, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->crop(50, 50, 0, 0);
                $img->save(public_path('uploads/thumb/'.$name));

                $page->image = $name;

            }

            $page->save();

            $response = [
                'message' => 'Page updated.',
                'data'    => $page->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('page.index');
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Page deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Page deleted.');
    }
}
