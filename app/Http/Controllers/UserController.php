<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {

        $users = User::paginate(20);

        return view('user.index', ['users' => $users]);
    }

    public function create(FormBuilder $formBuilder)
    {

        $form = $formBuilder->create(\App\Forms\CreateNewUser::class, [
            'method' => 'POST',
            'class'  => 'form-horizontal form-label-left',
            'url'    => route('logout')
        ]);

        return view('user.create', compact('form'));
    }

    public function store(Request $request)
    {
        User::create([
            'first_name' => $request->get('first_name'),
            'last_name'  => $request->get('last_name'),
            'email'      => $request->get('email'),
            'password'   => bcrypt($request->get('password'))
        ]);

        return redirect()->route('user.index');
    }

    public function edit($id)
    {

        $user = User::find($id);
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->first_name = $request->get('first_name');
        $user->last_name  = $request->get('last_name');
        $user->password   = bcrypt($request->get('password'));
        $user->save();

        return redirect()->route('user.index');
    }

    public function destroy($id)
    {

        $user = User::find($id);
        $user->delete();

        return redirect()->route('user.index');
        
    }

}
