<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PageRepository;
use Intervention\Image\Facades\Image as Image;

class PageController extends Controller
{

    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PageRepository $repository)
    {
       $this->middleware('auth');
       $this->repository = $repository;
    }

    public function index()
    {

        $pages = Page::with('user')->orderBy('created_at', 'desc')->paginate(10);

        return view('page.index', ['pages' => $pages]);
    }

    public function create()
    {
        return view('page.create');
    }

    public function store(Request $request)
    {

        $page = Page::create([
            'user_id' => Auth::id(),
            'title'        => $request->get('title'),
            'content'      => $request->get('content'),
            'publish_date' => date('Y-m-d', strtotime($request->get('publish_date')))
        ]);

        if ($request->hasFile('photo')) {

            $photo     = $request->file('photo');
            $name      = $photo->getClientOriginalName();
            $extension = $photo->getClientOriginalExtension();
            $name      = md5($name.time()).'.'.$extension;

            $img = Image::make($photo);
            $img->save(public_path('uploads/'.$name));

            $img->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->crop(50, 50, 0, 0);
            $img->save(public_path('uploads/thumb/'.$name));

            $page->image = $name;
            $page->save();

        }

        return redirect()->route('page.index');
    }

    public function edit($id)
    {
        $page = Page::find($id);
        return view('page.edit', ['page' => $page]);
    }

    public function update(Request $request, $id)
    {

        $page = Page::find($id);

        $page->title        = $request->get('title');
        $page->content      = $request->get('content');
        $page->publish_date = date('Y-m-d', strtotime($request->get('publish_date')));

        if ($request->hasFile('photo')) {

            $photo     = $request->file('photo');
            $name      = $photo->getClientOriginalName();
            $extension = $photo->getClientOriginalExtension();
            $name      = md5($name.time()).'.'.$extension;

            $img = Image::make($photo);
            $img->save(public_path('uploads/'.$name));

            $img->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->crop(50, 50, 0, 0);
            $img->save(public_path('uploads/thumb/'.$name));

            $page->image = $name;

        }

        $page->save();

        return redirect()->route('page.index');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();

        return redirect()->route('page.index');
    }

}
