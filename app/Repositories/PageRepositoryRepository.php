<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PageRepositoryRepository
 * @package namespace App\Repositories;
 */
interface PageRepositoryRepository extends RepositoryInterface
{
    //
}
