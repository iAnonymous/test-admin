<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PageRepositoryRepository;
use App\Entities\PageRepository;
use App\Validators\PageRepositoryValidator;

/**
 * Class PageRepositoryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PageRepositoryRepositoryEloquent extends BaseRepository implements PageRepositoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageRepository::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
